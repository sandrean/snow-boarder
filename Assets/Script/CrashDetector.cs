using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CrashDetector : MonoBehaviour
{
    [SerializeField] float crashDelay = 1f;
    [SerializeField] ParticleSystem crashEffect;
    [SerializeField] AudioClip crashSFX;
    bool singleCrash = true;
    void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Ground" && singleCrash) {
            FindObjectOfType<PlayerController>().DisableControls();
            crashEffect.Play();
            GetComponent<AudioSource>().PlayOneShot(crashSFX);
            singleCrash = false;
            Invoke("ReloadScene", crashDelay);
        }
    }

    void ReloadScene() {
        SceneManager.LoadScene("Level1");
    }
}
