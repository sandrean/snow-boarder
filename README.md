# Snow Boarder

## Description

A simple delivery game created while learning the C# Unity Game Developer 2D course by gamedev.tv. The intended purpose of this game was to teach me how to use cinemachine, surface effectors, simple particles, audio sources, and other Unity functions. The player controls a snowboarder, in which he moves forward automatically on his own. The player can tilt the snowboarder to a desired angle using the left and right arrow keys, and also increase the speed of the snowboarder using the up arrow key. The game is completed when the player crosses the finish line, signified by a red pole and a sound effect.

### Dependencies

- Unity v2020.3.22f1

### Executing program

Create a new project on unity. Add all the following files within the base folder of the unity project. Go to build settings using ctrl + shift + b to build the project to the desired settings in a new folder to generate the executable for the game.

